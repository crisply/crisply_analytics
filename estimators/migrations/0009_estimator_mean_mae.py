# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('estimators', '0008_estimator_trainer'),
    ]

    operations = [
        migrations.AddField(
            model_name='estimator',
            name='mean_mae',
            field=models.FloatField(default=0.0),
        ),
    ]
